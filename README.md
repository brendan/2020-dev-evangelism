# Brendan 2020 Developer Evangelism Plan

1. [Purpose](#purpose)
1. [Focus Areas](#focus)
    - 2.1 [Team: Dev in DevOps](#devops)
    - 2.2 [Team: Audience Focus](#audience)
    - 2.3 [Both Team & My Goal: Open Source & Relationship Building](#open-source)
    - 2.4 [My goal: CI/CD](#ci-cd)
1. [Events](#events)
1. [Ideas](#ideas)
    - 4.1 [Blogs](#blogs)
    - 4.2 [Talks](#talks)
    - 4.3 [Demos](#demos)
    - 4.4 [Useless is not Worthless](#useless)
    - 4.5 [Research & Learning](#research)
1. [Publishing Content](#publishing)
1. [Inspiration](#inspiration)

<a id="purpose"></a>
## Purpose

This document outlines my plans for 2020 as a Developer Evangelist at GitLab, those things I want to explore, and what I want to give back to the community.  It's not intended to be a comprehensive or up-to-date list of my work or priorities.  Those more tactical day-to-day items will be clearly articulated through the use of our [Technical Evangelism issue board](https://gitlab.com/groups/gitlab-com/-/boards/1565342?label_name[]=tech-evangelism).

It's also not the SSOT for what I *have* done any my body of work.  For that, I intend to use my site [boleary.dev](https://boleary.dev). That site serves as a transparent, easy to navigate way of seeing the entirety of what I've done (events I've attended, talks I've given, blogs I've written).

<a id="focus"></a>
## Focus Areas

<a id="devops"></a>
### Dev in DevOps

Goal: **Be seen as a role model for developers in nascent GitLab communities, spreading the gospel of git and git-enabled DevOps.**

#### Details
One of my team goals is to focus on the "dev" in DevOps.  I plan to leverage my experience in Product and Engineering Management, as well as hands-on experience in side-projects to promote how developers can be the critical link in the DevOps lifecycle.  Applying development best practices, source code management, and code review to what are traditionally thought of as "Ops" tasks can lead to organizations realizing the real value of DevOps.

Many of my blogs and videos will focus on these ideas, from thought leadership on topics like GitOps and "development practices in DevOps" like simple primatives to specific technical content around making DevOps actionable in the practical sense with code examples.

<a id="audience"></a>
### Audience Focus
A key focus for the team will be reaching nascent audiences that GitLab traditionally hasn't had a strong story around.  I will include a focus on Javascript - my strongest programming language, as well as a critical focus on .NET Core and the .NET framework.  Many of those developers are either traditionally Windows users or use .NET Core on Linux.  

* Javascript, JSConf, etc.
* .NET Core, .NET conferences
* Hacker News - follow relvant threads related to other audience focus area.

<a id="open-source"></a>
### Open Source & Relationship Building
#### Specific Focus
* Git / [Git 15th anniversary](https://gitlab.com/groups/gitlab-com/marketing/-/epics/679)
History: 1-3 blog posts about the history of git. Maybe one is focused on “the history of source control” and getting to git, one focused on the rise of git to be the dominate SCM and one on changes to git over the last 15 years (version 0.1 to 2.15)
Education: 15 videos for the 15th anniversary of Git: 15 quick (less than 5-7 minute) videos on learning git topics:
    - Rebase, merging
    - Reflog
    - Reverting
    - Cherry-picking
    - Amending commits
    - Branching, moving branches
    - [Fixup and autosquash](https://twitter.com/gtsiolis/status/1228330591235559425)
Thought Leadership: Interviewing open source community members about their thoughts on how Git has impacted the world over the last 15 years. Cascade into a LOT of content / reusable content.
* Vue.js - Vue 3 release, building with Vue, working with GitLab frontend team as we use more and more Vue.
* GitLab Runner Open Source Office Hours

#### General
* Apply to become a CNCF ambassador
* Watch CNCF data here: [Grafana](https://envoy.devstats.cncf.io/d/8/dashboards?orgId=1&refresh=15m)
* Meet with Siemans team who is dedicated to writing GitLab code

<a id="ci-cd"></a>
### CI/CD
GitLab CI/CD has always been near and dear to my heart.  From my days before coming to GitLab as a GitLab user, CI/CD is what made me fall in love with GitLab the product.  I genuinely believe that CI/CD the most critical thing for our users and customers to get right - as well as the decisive inflection point that allows folks to stop thinking of GitLab as "git" and instead as a complete DevOps platform delivered as a single application.

I've lived the pain of non-repeatable or hard-to-maintain CI/CD toolchains my entire career.  I've worked with all of the CI/CD tools on the market.  I've survived the torture of being in charge of a Jenkins installation.  I want to make better CI/CD tooling and more straightforward to manage pipelines the norm in our industry, which I believe requires modern tooling and changing the idea that Jenkins is the defacto CI platform.

<a id="events"></a>
## Events
My focus will be on the [owned TE events in this sheet](https://labwork.dev/go/te-event-sheet).  I've also added a number of comments for possible events this year to the second section.

To see events I've already planned on attending or have submitted to speak at, see [boleary.dev/events](https://boleary.dev/events) 

<a id="ideas"></a>
## Ideas

* Create a simple "business card" template with design that says "Be sure to come see our talk(s) at X time in X room" for conferences 
* Create a YouTube Tech Evangelism playlist with GitLab _and_ external videos we've help with

<a id="blogs"></a>
### Blogs
* Write more about git 2.25.0 https://github.blog/2020-01-13-highlights-from-git-2-25/
* GitLab CI/CD for Firebase - [see this blog issue](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6612)
* Write about partial clones - see [this](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/25430/diffs?commit_id=f619443d14b7da07bebb060f3daea92ec7b055af) and [this](https://docs.google.com/document/d/1hlS3qIT-v9Mk70mAEnLfBUanhF12BWf2XRFUwjOTkyk/edit)
* Buyer based open core page like “all-remote” pages
* DIYOps
* Zero to autoscaling runner in GCP in 4 minutes
* Sister Mary Keller, first female PhD in CS https://twitter.com/mit_csail/status/1206976768436887552?s=12
* There is no such thing as "not technical enough"
* Open source as a repository of knowledge- on the metal 3 Grassroots open
* Make a world drawing book -> svg -> design tool in GitLab
* Ansible home networking
* Caching docker images for DIND
* Turn GitLab flow video into a blog
* GitLab Review Apps for Netlify
* Turn Git-ing started with Git video into a blog
* After next Commit, blog on running conferences https://www.notion.so/boleary/Lessons-learned-from-going-to-a-lot-of-conferences-f9cd4d4543014137924fafbaff769f6b
* [Giflab](http://giflab.gitlab.io/) 
* As a developer, what do I REALLY need to know aboout Kubernetes
* GitLab "answer" to CircleCI Orbs or GitHub Actions (resuable code, templates, includes, etc)
* Retrospective on the [July 2009 cyberattacks - Wikipedia](https://en.wikipedia.org/wiki/July_2009_cyberattacks) in 2020.
* Make an npx about me (today's digital business card in JS)
* Pipeline optimization with "new" features: docker cache. Npm cache. DAG.  Basically the progress in CI/CD in the last ~year
* Open source highlighting blogs - half on the project half on its integration to GitLab.  A way to highlight open source projects / get it cross posted.
* GitLab CI/CD for retropie
* [Story of a single line of code costing 1/4 of a billion dolllars - 20 minutes into this podcast](https://podcasts.apple.com/us/podcast/on-the-metal/id1488187473?i=1000458467570)
* Update to [GitLab CI/CD for Vue.js](https://about.gitlab.com/blog/2017/09/12/vuejs-app-gitlab/) to answer [this](https://about.gitlab.com/blog/2017/09/12/vuejs-app-gitlab/#comment-4738957558) and other questions on it.

<a id="talks"></a>
### Talks
For my abstracts and previous talks, see [boleary.dev/talks](https://boleary.dev/talks)

* Write Lego JAMStack abstract (already have the talk)
* There is a talk to be had in [‎Arrested DevOps: Kubernetes & the Future on Apple Podcasts](https://podcasts.apple.com/us/podcast/arrested-devops/id773888088?i=1000458712547)
* “Simple primitives” talk (Linux -> android phone)
* “Say why not just what”
  - When you bring a tool in, have a good reason WHY. We brought it K8s because we had a thing that looks like a scheduler.
  - If everyone thinks their entire value is the thing they are doing right now then the human factors are different
  - Give you time back to do the stuff you know you want to do
* Talks on a given value or subvalue.
* Software for the buyer or for the user (concur effect)
* "A talk I'm not technical enough to give" (relates to blog, idea everyone can be technical enough)
* Lego JAMStack talk
* IBM —> Unix. Windows —> Linux (96). AwS —> ? On the metal ep 3 @ 50 min.

<a id="demos"></a>
### Demos
* Additional "short" videos like [Git-ing started with git](https://www.youtube.com/watch?v=Ce5nz5n41z4) and [GitLab flow](https://www.youtube.com/watch?v=InKNIvky2KE) that focus on one topic quickly and perform very well (14k and 34k respectively).
* Twich streaming?
* Annotated video with https://leipert-projects.gitlab.io/gitlab-history/?page=locAbsolute
* Build a CFP / editorial calendar app
* Rebuild iPhone launch but with GitLab
* Prometheus / grafana ansible role
* Crossplane.io with GitLab

<a id="useless"></a>
### [Useless is not Worthless](https://twitter.com/cassidoo/status/1225147255604137986)
> Name inspired by [Charlie Gerard's](https://charliegerard.github.io/) talk on Street Fighter and machine learning.

The concept of "useless is not worthless" is to make development fun and entertaining by creating projects and talking about things that go beyond your typical "here's how to create a Ruby app" type tutorials.  In this way, teaching and evangelizing git, git-enabled DevOps, and the Dev in DevOps becomes more engaging and relatable. 

I've already had some success with things like this, [Auto Breakfast for GitLab CI/CD](https://about.gitlab.com/blog/2018/06/29/introducing-auto-breakfast-from-gitlab/) and [Project management for home improvement projects](https://about.gitlab.com/blog/2018/02/08/using-gitlab-to-manage-house-renovation-priorities/).  This year I plan to lean into the success of those earlier posts in media, talks, and posts I create.  Some ideas include:

* Pinewood derby car Arduino to measure acceleration
* Pied piper demos
* Something visible/tangible (think Sentry Scouts, colored pins from partnerships). Seven sections of the tanuki badges to form the tanuki?
* Tanuki light that connects to MRs or pipelines or something
* [NASA Open APIs](https://api.nasa.gov/)
* [Kids jukebox](https://twitter.com/helenhousandi/status/1210677458393018369?s=12)
* [Snake on a Christmas tree](https://www.linkedin.com/feed/update/urn:li:activity:6615456802001817600/)
* Pipeline results on a Christmas tree
* The right abstractions live forever (Unix is everything now) so Gitlab ci/cd all the things (Pdp11 ci/cd then quantum computer on gcp)
* Package manager for slack emojis
* Automate Alexa light
* What to make for dinner app
* Mean girls tech blogs series
* Some IoT thing with [interstacks](https://www.interstacks.com/)
* Something with ML / nueral net and GitLab’s Slack’s custom emojis
* 7rings.dev revival
* Google [teachable machine](https://teachablemachine.withgoogle.com/) project

<a id="research"></a>
### Research & Learning

#### GitLab
* For each release: 5 tweets, 1 video, 1 blog
* Write up on monitoring

#### Technology
* Bring Developer perspective to Envoy, Prometheus and other CNCF projects
* Explore Flutter / mobile app development
* JS: Svelte, CSS animations / keyframes, web assembly now that it is the "fourth language of the web"
* [Answer the public](https://answerthepublic.com/reports/1e1bbfd4-37ae-46bf-990d-2014a10ddf08)

#### Evangelism
* [Developers Guide to Content](https://www.developersguidetocontent.com/)
* [Measuring Success and KPIs in Developer Relations](https://dev.to/tessamero/measuring-success-and-kpis-in-developer-relations-community-contributed-outline-1383)
* [Double Shipping](https://zachholman.com/posts/double-shipping)
* [DevRel collective](https://devrelcollective.fun)
* [Shouting at the Devops - Arresetd DevOps podcast on evangelism](https://podcasts.apple.com/us/podcast/arrested-devops/id773888088?i=1000425055037)
* [How to Stop Saying “Um,” “Ah,” and “You Know”](https://hbr.org/2018/08/how-to-stop-saying-um-ah-and-you-know)
* [Personal Branding](https://mention.com/en/blog/twitter-personal-branding/)
* [Twitter Marketing](https://copyblogger.com/ultimate-twitter/)
* [Beginners guide to Twitter](https://michaelhyatt.com/the-beginners-guide-to-twitter/)
* [Growth Design Case Studies](https://growth.design/case-studies/)
* [How to write a talk proposal](https://www.usenix.org/blog/how-write-talk-proposal)
* [Things people want out of a CFP](https://twitter.com/sarah_edo/status/1210643980011991042?s=12)
* [Pixar in a box - the art of storytelling](https://www.khanacademy.org/partner-content/pixar/storytelling)
* [Back in the story spine](https://www.aerogrammestudio.com/2013/06/05/back-to-the-story-spine/)
* [My Interview and Podcast Production Process on the Hanselminutes Podcast - Scott Hanselman](https://www.hanselman.com/blog/MyInterviewAndPodcastProductionProcessOnTheHanselminutesPodcast.aspx)
* [This thread on giving a conference talk](https://threadreaderapp.com/thread/1215710451343904768.html)
* [Learn with Jason](https://www.learnwithjason.dev/) from [Jason Lengstorf](https://twitter.com/jlengstorf) of Netlify
* Creative mornings

<a id="publishing"></a>
## Publishing Content

### My Platforms
* [Twitter](https://twitter.com/olearycrew)
* [GitLab Blog](https://about.gitlab.com/blog/)
* [My blog](https://boleary.dev/blog)
* [Medium](https://medium.com/olearycrew)
* [Dev.to](https://dev.to/olearycrew)
* [GitLab on Reddit](https://www.reddit.com/r/gitlab/)

### External Sources
* New Stack
* DevOps.com
* Partner blogs (AWS, Google)
* Google / GCP - Nathen Harvey

<a id="inspiration"></a>
## Inspiration

### Talks & Blogs
* [AWS re:Invent 2019: Reinvent your pipeline with GitLab, Kubernetes, and Amazon EKS](https://youtu.be/Eeil2VBsgT8)
* [DevSecOps & GitLab's Security Solutions](https://youtu.be/CjX1TsCZgoQ)
* [AWS re:Invent 2019: Amplifying CI/CD helped Wag! reduce release process by 600%](https://youtu.be/HfEl9GXZC0s)
* [Sample Conference Proposals - O'Reilly Media](https://www.oreilly.com/conferences/sample_proposals.html)
* [What is the most inspiring tech talk of 2019?](https://dev.to/etiennedepaulis/what-is-the-most-inspiring-tech-talk-of-2019-4e03)

### Tools
* [Slides Code Highlighter](https://romannurik.github.io/SlidesCodeHighlighter/)
* [Do it live](https://doitlive.readthedocs.io/en/stable/)
* [Loop back Mac](https://rogueamoeba.com/loopback/)
* [Polls for the start of talks](https://pollev.com/)

### People
* [Kelsey Hightower](https://twitter.com/kelseyhightower)
* [Nathen Harey](https://twitter.com/nathenharvey)
* [Jessie Frazelle](https://blog.jessfraz.com/)
* [Chloe Codon](https://twitter.com/ChloeCondon)
* [Scott Hanselman](https://www.hanselman.com/)
* [Emily Freeman](https://emilyfreeman.io/)
* [Sarah Drasner](https://twitter.com/sarah_edo)
* [Matt Stratton](https://twitter.com/mattstratton)
* [Corey Quinn](https://twitter.com/QuinnyPig)
* [Ali Spittel](https://www.alispit.tel/)

